const express = require('express');
const upload = require('../upload');
const Album = require('../models/Album');
const Track = require('../models/Track');

const router = express.Router();

router.get('/', (req, res) => {
  const artist = req.query.artist;

  Album.find(artist ? {artist} : null)
    .then(albums => res.send(albums))
    .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {
  Album.findById(req.params.id).populate('artist')
    .then(album => {
      if (album) res.send(album);
      else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
});

router.post('/', upload.single('image'), (req, res) => {
  const request = req.body;

  if (req.body.tracks) {
    const {tracks, ...albumData} = request;

    if (req.file) {
      albumData.image = req.file.filename;
    }

    const album = new Album(albumData);

    album.save()
      .then(async result => {
        const tracksData = JSON.parse(tracks).map(track => ({...track, album: result._id}));

        const tracksResponse = [];

        for(const data of tracksData) {
          await new Track(data).save()
            .then(trackResponse => tracksResponse.push(trackResponse))
            .catch(error => res.status(400).send(error));
        }

        res.send({...result._doc, tracks: tracksResponse});
      })
      .catch(error => res.status(400).send(error));
  } else res.sendStatus(400);

});


module.exports = router;
