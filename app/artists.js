const express = require('express');
const upload = require('../upload');
const Artist = require('../models/Artist');

const router = express.Router();

router.get('/', (req, res) => {
  Artist.find()
    .then(artists => res.send(artists))
    .catch(() => res.sendStatus(500));
});

router.post('/', upload.single('image'), ((req, res) => {
  const artistData = req.body;

  if (req.file) {
    artistData.image = req.file.filename;
  }

  const artist = new Artist(artistData);

  artist.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
}));

module.exports = router;