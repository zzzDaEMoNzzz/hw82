const mongoose = require('mongoose');
const config = require('./config');

const Album = require('./models/Album');
const Artist = require('./models/Artist');
const Track = require('./models/Track');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const [neffex, hollywood_undead] = await Artist.create(
    {name: 'Neffex', description: 'Hip-Hop/Rock band from Los Angeles'},
    {name: 'Hollywood Undead', description: 'Hollywood Undead is an American rap rock band from Los Angeles, California, formed in 2005'},
  );

  const [bom, five] = await Album.create(
    {name: 'Best of Me: The Collection', artist: neffex._id, date: '2019'},
    {name: 'Five', artist: hollywood_undead._id, date: '2017'}
  );

  await Track.create(
    {name: 'Best of Me', album: bom._id, duration: 227},
    {name: 'Grateful', album: bom._id, duration: 182},
    {name: 'Numb', album: bom._id, duration: 144},
    {name: 'California Dreaming', album: five._id, duration: 234},
    {name: 'Whatever It Takes', album: five._id, duration: 187},
    {name: 'Bad Moon', album: five._id, duration: 232}
  );

  return connection.close();
};

run().catch(error => {
  console.error('Something wrong happened...', error);
});